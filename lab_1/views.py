from django.shortcuts import render

#lab1
#import datetime
# Enter your name here
#mhs_name = 'Nabilla Hariyana' # TODO Implement this
#
# Create your views here.
# def index(request):
#     response = {
#         'name': mhs_name,
#         'age' : calculate_age(1998)
#         }
#     return render(request, 'index.html', response)
#
# # TODO Implement this to complete last checklist
# def calculate_age(birth_year):
#     tahunSekarang = datetime.datetime.today().year
#     return tahunSekarang - birth_year




#lab2
from datetime import datetime, date
# Enter your name here
mhs_name = 'Nabilla Hariyana' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 6, 6) #TODO Implement this, format (Year, Month, Date)
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
