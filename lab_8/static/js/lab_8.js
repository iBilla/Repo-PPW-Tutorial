// FB initiation function
window.fbAsyncInit = function() {
  FB.init({
    appId      : '139414663492970',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

FB.AppEvents.logPageView();

FB.getLoginStatus(function(response) {
  console.log(response)
  const loginFlag = response.status === 'connected';
  render(loginFlag);
});
};

// Call init facebook. default dari facebook
(function(d, s, id){
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) {return;}
 js = d.createElement(s); js.id = id;
 js.src = "https://connect.facebook.net/en_US/sdk.js";
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
// dan jalankanlah fungsi render di bawah, dengan parameter true jika
// status login terkoneksi (connected)

// Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
// otomatis akan ditampilkan view sudah login

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
if (loginFlag) {
  // Jika yang akan dirender adalah tampilan sudah login

  // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
  // yang menerima object user sebagai parameter.
  // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
  getUserData(user => {
    // Render tampilan profil, form input post, tombol post status, dan tombol logout
    $('#lab8').html(
      '<div class="profile">' +
        '<div class="photoContainer" style="background-image: url('+user.cover.source+'); width: 600px; margin-left: auto; margin-right: auto;">' +
        '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
        '<div class="data">' +
          '<h1>' + user.name + '</h1>' +
          '<h2>' + user.about + '</h2>' +
          '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
        '</div>' +
      '</div>' +
      '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
      '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
      '<button class="logout" onclick="facebookLogout()">Logout</button>'
    );

    // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
    // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
    // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
    // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
    getUserFeed(feeds => {
      feeds.data.map(feed => {
        // Render feed, kustomisasi sesuai kebutuhan.
        if (feed.message && feed.story) {
          $('#lab8').append(
            '<div class="feed" id="' + feed.id + '">' +
              '<h1>' + feed.message + '</h1>' +
              '<h2>' + feed.story + '</h2>' +
            '</div>'
          );
        } else if (feed.message) {
          $('#lab8').append(
            '<div class="feed" id="' + feed.id + '">' +
              '<h1>' + feed.message + '</h1>' +
            '</div>'
          );
        } else if (feed.story) {
          $('#lab8').append(
            '<div class="feed" id="' + feed.id + '">' +
              '<h2>' + feed.story + '</h2>' +
            '</div>'
          );
        }
        button = '<button id="' + feed.id + '" onclick="deleteFeed(event)"> Delete </button>';
        $('#lab8').append(button);
      })
    })
  })
} else {
  // Tampilan ketika belum login
  $('#lab8').html('<img src="http://newsroom.niu.edu/wp-content/uploads/2015/03/all_friends-512.png", width="200rem;", style="margin-top:65px;"><br><br><button class="btn btn-primary btn-lg login" onclick="facebookLogin()">Log In</button>');
}
};

const facebookLogin = () => {
// TODO: Implement Method Ini
// Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
// ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
// pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
FB.login(function(response){
  console.log(response);
  render(response);
}, {scope:'public_profile, email, user_about_me, user_posts, publish_actions'})
};

const facebookLogout = () => {
// TODO: Implement Method Ini
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      FB.logout();
      console.log('logout success');
      render(false);
    }
 });

// Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
// ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
};


// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
FB.api('/me?fields=id,name,about,email,gender,cover,picture', 'GET', function(response){
  console.log(response);
  fun(response);
});
};

const getUserFeed = (fun) => {
// TODO: Implement Method Ini
// Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
// yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
// tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
// tersebut
FB.api('/me/feed', function(response) {
  console.log('feeds', response)
  fun(response);
});
};

const deleteFeed = (e) => {
console.log(e.target)
FB.api(
  "/"+e.target.id+"",
  "DELETE",
  function (response) {
    console.log(response)
    if (response && !response.error) {
      alert('This post is deleted');
      e.target.parentNode.style.display = 'none';
      window.location.reload();
    }
  }
);
};

const postFeed = (message) => {
// Todo: Implement method ini,
// Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
// Melalui API Facebook dengan message yang diterima dari parameter.
FB.api('/me/feed', 'POST', {message:message}, function(response) {
  if (!response || response.error) {
    console.log('Error occured');
  } else {
    console.log('Post ID: ' + response.id);
  }
});
$('#postInput').val('');
window.location.reload();
};

const postStatus = () => {
const message = $('#postInput').val();
postFeed(message);
};
