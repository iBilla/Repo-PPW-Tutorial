from django.contrib import admin
from .models import Friend, Mahasiswa

# Register your models here.
admin.site.register(Friend)
admin.site.register(Mahasiswa)
